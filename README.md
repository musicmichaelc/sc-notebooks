# Supercollider notebooks

This is a little collection of Supercollider code I have used before and / or am using in musical projects, organized in Jupyter notebooks using [sckernel](https://github.com/andrewdavis33/sckernel "Check out the sckernel repository").  

Regular `.scd` files _(autoconverted by `sckernel`)_ are availible as a [zipped file](https://musicmichaelc.gitlab.io/sc-notebooks/sc-notebooks-scd.zip "Download .zip file").

## Disclaimer

So far, none of these notebooks constitute  a complete and self-contained musical composition, but are rather intended  as parts  of a bigger whole&mdash;a composition / improvisation / live performance / recording.  

Usually, I have used these in the context of performing / recording something alone or with others while  playing on acoustic instruments, at certain times adding electronic sounds (e.g. with SuperCollider) to interact with and/or to enrich the performance.  



## Prerequisites  

Running this code requires a local [installation of SuperCollider](https://supercollider.github.io/download "Click here to go to the 'Download' page of SuperCollider") (unfortunately, it won&rsquo;t run interactively in the browser&mdash;such as on [mybinder.org](https://mybinder.org)&mdash;at least I haven&rsquo;t found a way to do that up to now).    

### SC-Kernel&mdash;a very helpful tool

I can highly recommend  [sckernel](https://github.com/andrewdavis33/sckernel "Go to the sckernel repository")&mdash;I have found it to be very useful for organizing  thoughts and ideas, musically and code-wise. It is a tool that lets one run SuperCollider code inside the code cells of Jupyter notebooks, additionally providing syntax highlighting as well as a post window to see the output from SuperCollider.   



## Viewing the notebooks in the browser

The notebooks can also be displayed directly in GitLab, however, the SuperCollider specific syntax coloring doesn&rsquo;t work&mdash;at least I haven&rsquo;t found a way to implement it yet.   


