{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Rustling Wind"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Part of my **[SuperCollider notebooks](https://gitlab.com/musicmichaelc/sc-notebooks \"Click here to go to the repository home of the SuperCollider notebooks\")**  \n",
    "Copyright &copy; 2021  Michael Christensen\n",
    "\n",
    "These notebooks are free software: you can redistribute and/or modify\n",
    "    them under the terms of the [GNU Affero General Public License](LICENSE) as published\n",
    "    by the Free Software Foundation, either version 3 of the License, or\n",
    "    (at your option) any later version.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Initialization and basic setup\n",
    "\n",
    "Start the audio server, set up various options and display basic meters, freqscope and controls.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Display I/O devices and allocate more memory\n",
    "\n",
    "I doubled the usual amount of memory allocated to the SCServer _(8192K to 16384K)_ because  the Reverb/Delay Synth had initially failed due to insufficient memory. \n",
    "The allocation size must be set before booting the server&mdash;and there is a good reason for this, according to [this wiki](http://supercollider.sourceforge.net/wiki/index.php/User_FAQ#FAILURE_.2Fs_new_alloc_failed.2C_increase_server.27s_memory_allocation_.28e.g._via_ServerOptions.29 \"View the original source of the quote\"):\n",
    "> This is because direct allocation from the OS, by functions such as `malloc()`, is not real-time safe. The OS may take too long to return the new block, causing glitches in the audio. To solve this problem, the server allocates a chunk of memory when it starts up and parcels it out to unit generators as needed.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": []
    }
   ],
   "source": [
    "s = Server.default;\n",
    "\n",
    "(\"Availible Audio I/O Devices: \" + ServerOptions.devices).postln;\n",
    "s.options.memSize_(16384); // allocate twice the default memory size\n",
    "(s.options.memSize + \"K memory will be allocated to the Server on its next boot.\").postln;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following line of code is optional&mdash;e.g. for recording the screen with audio:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": []
    }
   ],
   "source": [
    "s.options.outDevice_(\"Soundflower (2ch)\"); // An aggregate device to capture audio during screen recording"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Boot/reboot server and display basic control windows"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": []
    }
   ],
   "source": [
    "s.reboot;\n",
    "\n",
    "s.meter;\n",
    "s.makeGui;\n",
    "s.freqscope;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setup Synths and GUI-controls\n",
    "\n",
    "### Create SynthDefs\n",
    "\n",
    "1. Wind: `\"wind\"`\n",
    "2. Reverb & Delay: `\\FreeVerb2x2DelayC`\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": []
    }
   ],
   "source": [
    "SynthDef(\"wind\", \n",
    "  { |out, \n",
    "    fcModFreq=0.1, fcRange=#[50, 1200], // !!Range must not get close to zero!! \n",
    "    rqModFreq=0.04, rqRange=#[0.3, 0.5], \n",
    "    bal=0.0, amp=0.4,\n",
    "    gate=1|\n",
    "\n",
    "    var fcMin = fcRange[0], fcMax = fcRange[1];\n",
    "    var rqMin = rqRange[0], rqMax = rqRange[1];\n",
    "\n",
    "    var sound = { RLPF.ar(\n",
    "      in: WhiteNoise.ar(0.1),\n",
    "      freq: LFDNoise3.kr(freq: fcModFreq, mul: (fcMax-fcMin)/2, add: (fcMax+fcMin)/2),\n",
    "      rq: LFDNoise3.kr(freq: rqModFreq, mul: (rqMax-rqMin)/2, add: (rqMax+rqMin)/2),\n",
    "      mul: amp\n",
    "    ).softclip}.dup(2);\n",
    "\n",
    "    sound = Balance2.ar(sound[0], sound[1], bal).softclip;\n",
    "\n",
    "    sound = sound * EnvGen.kr(Env.cutoff, gate, doneAction: Done.freeSelf);\n",
    "    Out.ar(out, sound);\n",
    "  }, [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0]\n",
    ").add;\n",
    "\n",
    "// FreeVerb2 - demo synthdef\n",
    "SynthDef(\\FreeVerb2x2DelayC, \n",
    "  { |out, \n",
    "    mix=0.25, room=0.15, damp=0.5, amp=1.0, // FreeVerb2 args\n",
    "    i_max_delay=12, delay=2.4, delayAmp=0.3,\n",
    "    gate=1|\n",
    "    \n",
    "    var signal;\n",
    "\n",
    "    signal = In.ar(out, 2);\n",
    "    \n",
    "    signal = FreeVerb2.ar( // FreeVerb2 - true stereo UGen\n",
    "      signal[0], // Left channel\n",
    "      signal[1], // Right Channel\n",
    "      mix, room, damp, amp\n",
    "    ).softclip;\n",
    "    \n",
    "    signal = DelayC.ar(\n",
    "      in: signal, \n",
    "      maxdelaytime: i_max_delay, \n",
    "      delaytime: delay, \n",
    "      mul: delayAmp, \n",
    "      add: signal // include (keep) original signal by adding it to the delay\n",
    "    ).softclip;\n",
    "    \n",
    "    signal = signal * EnvGen.kr(Env.cutoff, gate, doneAction: Done.freeSelf);\n",
    "    ReplaceOut.ar(out, signal); \n",
    "  }, [0.1, 0.1, 0.1, 0.1, 0.1, 0, 0.3, 0]\n",
    ").add;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create GUI-Controls for Wind"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": []
    }
   ],
   "source": [
    "// Essentials:\n",
    "var node, cmdPeriodFunc;\n",
    "\n",
    "// Gui Elements:\n",
    "var w, startBtn;\n",
    "var fcModCtl, fcRangeCtl;\n",
    "var rqModCtl, rqRangeCtl;\n",
    "var balanceCtl, ampCtl;\n",
    "\n",
    "// Gui sizing/appearance:\n",
    "var numOfCtls = 7;\n",
    "var ctlHeight = 25; // leaves a little space around the text---looks better\n",
    "var wHeight = (ctlHeight + 5) * numOfCtls;\n",
    "var labelWidth = 100, numWidth = 60;\n",
    "\n",
    "  \n",
    "// make the window\n",
    "w = Window(\"Wind\", Rect(20, 400, 440, wHeight));\n",
    "w.front; // make window visible and bring to front\n",
    "w.view.decorator = FlowLayout(w.view.bounds);\n",
    "w.view.decorator.gap=2@2;\n",
    "\n",
    "// add a button to start and stop the sound.\n",
    "startBtn = Button(w, labelWidth @ ctlHeight);\n",
    "startBtn.states = [\n",
    "  [\"Start\", Color.black, Color.green(0.7)],\n",
    "  [\"Stop\", Color.white, Color.red(0.7)]\n",
    "];\n",
    "startBtn.action = {|view|\n",
    "  if (view.value == 1) {\n",
    "    // start sound\n",
    "    node = Synth( \"wind\", [\n",
    "      \"fcModFreq\", fcModCtl.value,\n",
    "      \"fcRange\", fcRangeCtl.value,\n",
    "      \"rqModFreq\", rqModCtl.value,\n",
    "      \"rqRange\", rqRangeCtl.value,\n",
    "      \"bal\", balanceCtl.value,\n",
    "      \"amp\", ampCtl.value.dbamp ]);\n",
    "  } {\n",
    "      // set gate to zero to cause envelope to release\n",
    "      node.release; node = nil;\n",
    "  };\n",
    "};\n",
    "\n",
    "// create controls for all parameters\n",
    "w.view.decorator.nextLine;\n",
    "fcModCtl = EZSlider(w, 430 @ ctlHeight, \"Cutoff Mod \", \n",
    "  ControlSpec(0.03, 2.0, \\lin, step: 0.01, default: 0.29, units: \\Hz),\n",
    "  {|ez| node.set( \"fcModFreq\", ez.value )}, \n",
    "  labelWidth: labelWidth,\n",
    "  numberWidth: numWidth,\n",
    "  unitWidth:30)\n",
    "    .setColors(Color.grey,Color.white, Color.grey(0.7),Color.grey, Color.white, Color.yellow);\n",
    "\n",
    "w.view.decorator.nextLine;\n",
    "fcRangeCtl = EZRanger(w, 430 @ ctlHeight, \"Cutoff Range \", \n",
    "  ControlSpec(50, 1400, \\exp, step: 5.0, default: [300, 850], units: \\Hz),\n",
    "  {|ez| node.set( \"fcRange\", ez.value )}, \n",
    "  initVal: [95, 855], // Setting the default value via ControlSpec didn't work, so maybe this will\n",
    "  labelWidth: labelWidth,\n",
    "  numberWidth: numWidth,\n",
    "  unitWidth:30)\n",
    "    .setColors(Color.grey,Color.white, Color.grey(0.7),Color.grey, Color.white, Color.yellow);\n",
    "\n",
    "w.view.decorator.nextLine;\n",
    "rqModCtl = EZSlider(w, 430 @ ctlHeight, \"Rq Mod \", \n",
    "  ControlSpec(0.01, 0.99, \\lin, step: 0.01, default: 0.07, units: \\Hz),\n",
    "  {|ez| node.set( \"rqModFreq\", ez.value )}, \n",
    "  labelWidth: labelWidth,\n",
    "  numberWidth: numWidth,\n",
    "  unitWidth:30)\n",
    "    .setColors(Color.grey,Color.white, Color.grey(0.7),Color.grey, Color.white, Color.yellow);\n",
    "\n",
    "w.view.decorator.nextLine;\n",
    "rqRangeCtl = EZRanger(w, 430 @ ctlHeight, \"Rq Range \", \\rq,\n",
    "  {|ez| node.set( \"rqRange\", ez.value )}, \n",
    "  initVal: [0.39, 0.67], // ControlSpec's default value didn't work, so we'll try to add it here \n",
    "  labelWidth: labelWidth,\n",
    "  numberWidth: numWidth,\n",
    "  unitWidth:30)\n",
    "    .setColors(Color.grey,Color.white, Color.grey(0.7),Color.grey, Color.white, Color.yellow);\n",
    "\n",
    "w.view.decorator.nextLine;\n",
    "balanceCtl = EZSlider(w, 430 @ ctlHeight, \"Balance \", \\bipolar,\n",
    "  {|ez| node.set( \"bal\", ez.value )}, \n",
    "  initVal: 0.0,\n",
    "  labelWidth: labelWidth,\n",
    "  numberWidth: numWidth,\n",
    "  unitWidth:30)\n",
    "    .setColors(Color.grey,Color.white, Color.grey(0.7),Color.grey, Color.white, Color.yellow);\n",
    "\n",
    "w.view.decorator.nextLine;\n",
    "ampCtl = EZSlider(w, 430 @ ctlHeight, \"Amplitude \", \\db,\n",
    "  {|ez| node.set( \"amp\", ez.value.dbamp )}, \n",
    "  initVal: 0.0, \n",
    "  labelWidth: labelWidth,\n",
    "  numberWidth: numWidth,\n",
    "  unitWidth:30)\n",
    "    .setColors(Color.grey,Color.white, Color.grey(0.7),Color.grey, Color.white, Color.yellow);\n",
    "\n",
    "\n",
    "// set start button to zero upon a cmd-period\n",
    "cmdPeriodFunc = { startBtn.value = 0; };\n",
    "CmdPeriod.add(cmdPeriodFunc);\n",
    "\n",
    "// stop the sound when window closes and remove cmdPeriodFunc.\n",
    "w.onClose = {\n",
    "  node.free; node = nil;\n",
    "  CmdPeriod.remove(cmdPeriodFunc);\n",
    "};"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create GUI-controls for Reverb/Delay"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": []
    }
   ],
   "source": [
    "// Essentials/settings:\n",
    "var node, cmdPeriodFunc;\n",
    "var maxDelay = 16;\n",
    "\n",
    "// Gui Elements:\n",
    "var w, startBtn;\n",
    "var mixCtl, roomCtl, dampCtl, ampCtl; // FreeVerb2 ctls\n",
    "var delayCtl, delayAmpCtl; // DelayC ctls\n",
    "\n",
    "// Gui sizing/appearance:\n",
    "var numOfCtls = 7;\n",
    "var ctlHeight = 25; // leaves a little space around the text---looks better\n",
    "var wHeight = (ctlHeight + 5) * numOfCtls;\n",
    "var labelWidth = 100, numWidth = 60;\n",
    "\n",
    "  \n",
    "// make the window\n",
    "w = Window(\"Reverb/Delay\", Rect(20, 150, 440, wHeight));\n",
    "w.front; // make window visible and bring to front\n",
    "w.view.decorator = FlowLayout(w.view.bounds);\n",
    "w.view.decorator.gap=2@2;\n",
    "\n",
    "// add a button to start and stop the sound.\n",
    "startBtn = Button(w, labelWidth @ ctlHeight);\n",
    "startBtn.states = [\n",
    "  [\"Start\", Color.black, Color.green(0.7)],\n",
    "  [\"Stop\", Color.white, Color.red(0.7)]\n",
    "];\n",
    "startBtn.action = {|view|\n",
    "  if (view.value == 1) {\n",
    "    // start sound\n",
    "    node = Synth( \\FreeVerb2x2DelayC, [\n",
    "        \"mix\", mixCtl.value,\n",
    "        \"room\", roomCtl.value,\n",
    "        \"damp\", dampCtl.value,\n",
    "        \"amp\", ampCtl.value.dbamp,\n",
    "        \"i_max_delay\", maxDelay,\n",
    "        \"delay\", delayCtl,\n",
    "        \"delayAmp\", delayAmpCtl.value.dbamp ],\n",
    "      addAction:\\addToTail\n",
    "    );\n",
    "  } {\n",
    "    // set gate to zero to cause envelope to release\n",
    "    node.release; node = nil;\n",
    "  };\n",
    "};\n",
    "\n",
    "// create controls for all parameters\n",
    "w.view.decorator.nextLine;\n",
    "mixCtl = EZSlider(w, 430 @ ctlHeight, \"Mix Dry/Wet \", \\unipolar,\n",
    "  {|ez| node.set( \"mix\", ez.value )}, \n",
    "  initVal: 0.5, \n",
    "  labelWidth: labelWidth,\n",
    "  numberWidth: numWidth,\n",
    "  unitWidth:30)\n",
    "    .setColors(Color.grey,Color.white, Color.grey(0.7),Color.grey, Color.white, Color.yellow);\n",
    "\n",
    "w.view.decorator.nextLine;\n",
    "roomCtl = EZSlider(w, 430 @ ctlHeight, \"Room Size \", \\unipolar,\n",
    "  {|ez| node.set( \"room\", ez.value )}, \n",
    "  initVal: 0.77,\n",
    "  labelWidth: labelWidth,\n",
    "  numberWidth: numWidth,\n",
    "  unitWidth:30)\n",
    "    .setColors(Color.grey,Color.white, Color.grey(0.7),Color.grey, Color.white, Color.yellow);\n",
    "\n",
    "w.view.decorator.nextLine;\n",
    "dampCtl = EZSlider(w, 430 @ ctlHeight, \"Damp \", \\unipolar,\n",
    "  {|ez| node.set( \"damp\", ez.value )}, \n",
    "  initVal: 0.26,\n",
    "  labelWidth: labelWidth,\n",
    "  numberWidth: numWidth,\n",
    "  unitWidth:30)\n",
    "    .setColors(Color.grey,Color.white, Color.grey(0.7),Color.grey, Color.white, Color.yellow);\n",
    "\n",
    "w.view.decorator.nextLine;\n",
    "ampCtl = EZSlider(w, 430 @ ctlHeight, \"Reverb amp \", \\db,\n",
    "  {|ez| node.set( \"amp\", ez.value.dbamp )}, \n",
    "  initVal: 0, \n",
    "  labelWidth: labelWidth,\n",
    "  numberWidth: numWidth,\n",
    "  unitWidth:30)\n",
    "    .setColors(Color.grey,Color.white, Color.grey(0.7),Color.grey, Color.white, Color.yellow);\n",
    "\n",
    "w.view.decorator.nextLine;\n",
    "delayCtl = EZSlider(w, 430 @ ctlHeight, \"Delay \", \n",
    "  ControlSpec(0.0, maxDelay, \\lin, step: 0.1, default: 6.7, units: \\sec),\n",
    "  {|ez| node.set( \"delay\", ez.value )}, \n",
    "  labelWidth: labelWidth,\n",
    "  numberWidth: numWidth,\n",
    "  unitWidth:30)\n",
    "    .setColors(Color.grey,Color.white, Color.grey(0.7),Color.grey, Color.white, Color.yellow);\n",
    "\n",
    "w.view.decorator.nextLine;\n",
    "delayAmpCtl = EZSlider(w, 430 @ ctlHeight, \"Delay amp \", \\db,\n",
    "  {|ez| node.set( \"delayAmp\", ez.value.dbamp )}, \n",
    "  initVal: -2.18, \n",
    "  labelWidth: labelWidth,\n",
    "  numberWidth: numWidth,\n",
    "  unitWidth:30)\n",
    "    .setColors(Color.grey,Color.white, Color.grey(0.7),Color.grey, Color.white, Color.yellow);\n",
    "\n",
    "\n",
    "// set start button to zero upon a cmd-period\n",
    "cmdPeriodFunc = { startBtn.value = 0; };\n",
    "CmdPeriod.add(cmdPeriodFunc);\n",
    "\n",
    "// stop the sound when window closes and remove cmdPeriodFunc.\n",
    "w.onClose = {\n",
    "  node.free; node = nil;\n",
    "  CmdPeriod.remove(cmdPeriodFunc);\n",
    "};"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Cleanup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": []
    }
   ],
   "source": [
    "Window.closeAll;"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "SC_Kernel",
   "language": "text",
   "name": "sckernel"
  },
  "language_info": {
   "codemirror_mode": "sclang",
   "name": "sclang"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
